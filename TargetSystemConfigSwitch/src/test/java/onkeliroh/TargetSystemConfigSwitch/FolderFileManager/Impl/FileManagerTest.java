/**
 * 
 */
package onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author onkeliroh
 *
 */
public class FileManagerTest {

	private static final String TO_BE_REWRITEN_TEST_FILE = "toberewritentestfile";
	private static final String GREAT_SUCCESS = "Great Success";
	private static final String REGEX_PATTERN_DEV_SYS = "[A-Z]{2,3}(-|_)[A-Z]{2,3}";
	private static final String EMPTY_SOURCE_TEST_FILE = "emptysourcetestfile";
	private static final String TEST_SOURCE_FILE_NAME = "testSourceFile";
	private static final String TEST_TARGET_FILE_NAME = "testTargetFile";
	private File testTargetFile;
	private String tmpFilesFolder;
	private List<File> testSourceFileList;
	private File tmpTargetFolderPath;
	private File emptySourceTestFile;
	private File toBeRewritenTestFile;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		testTargetFile = File.createTempFile(TEST_TARGET_FILE_NAME, "");
		emptySourceTestFile = File.createTempFile(EMPTY_SOURCE_TEST_FILE, "");
		toBeRewritenTestFile= File.createTempFile(TO_BE_REWRITEN_TEST_FILE, "");

		tmpFilesFolder = testTargetFile.getAbsolutePath().substring(0,
				testTargetFile.getAbsolutePath().lastIndexOf(File.separator));
		String tmpTargetFilesFolder = tmpFilesFolder + File.separator + "targets" + File.separator;
		tmpTargetFolderPath = new File(tmpTargetFilesFolder);
		tmpTargetFolderPath.mkdirs();

		testSourceFileList = new ArrayList<File>();

		for (int i = 0; i < 10; i++) {
			File tmpTestSourceFile = File.createTempFile(TEST_SOURCE_FILE_NAME + Integer.toString(i), "",
					tmpTargetFolderPath);
			testSourceFileList.add(tmpTestSourceFile);
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(tmpTestSourceFile))) {
				bw.write("#######\n");
				bw.write("RC_RTS");

				bw.flush();
				bw.close();
			} catch (IOException e) {
				throw e;
			}
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		for (File f : testSourceFileList) {
			f.delete();
		}
		testTargetFile.delete();
		tmpTargetFolderPath.delete();
		emptySourceTestFile.delete();
		toBeRewritenTestFile.delete();
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl.FileManager#scanFileForPattern(java.lang.String, java.lang.String)}.
	 */
	@Test
	public final void testScanFileForPattern() throws Exception {
		FileManager fm = new FileManager();

		int foundNTimes = 0;

		for (File f : testSourceFileList) {
			if (fm.scanFileForPattern(f.getAbsolutePath(), REGEX_PATTERN_DEV_SYS)) {
				foundNTimes++;
			}
		}
		Assert.assertEquals(testSourceFileList.size(), foundNTimes);

		Assert.assertFalse(fm.scanFileForPattern(emptySourceTestFile.getAbsolutePath(), REGEX_PATTERN_DEV_SYS));
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl.FileManager#getStringMatchingPatternFromFile(java.lang.String, java.lang.String)}.
	 */
	@Test
	public final void testGetStringMatchingPatternFromFile() throws Exception {
		FileManager fm = new FileManager();

		int foundNTimes = 0;

		for (File f : testSourceFileList) {
			String line = fm.getStringMatchingPatternFromFile(f.getAbsolutePath(), REGEX_PATTERN_DEV_SYS);
			if (line != null) {
				foundNTimes++;
			}
		}
		Assert.assertEquals(testSourceFileList.size(), foundNTimes);
		Assert.assertFalse(fm.scanFileForPattern(emptySourceTestFile.getAbsolutePath(), REGEX_PATTERN_DEV_SYS));
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl.FileManager#replaceStringInFile(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public final void testReplaceStringInFile() throws Exception {
		List<String> content = new ArrayList<String>();
		final String BLAA_STRING = "Blaa";
		content.add(BLAA_STRING);
		Files.write(toBeRewritenTestFile.toPath(),content);
		
		FileManager fm = new FileManager();
		fm.replaceStringInFile(toBeRewritenTestFile.getAbsolutePath(), "[A-Za-z]{4}", GREAT_SUCCESS);
		
		List<String> fileContent = new ArrayList<>(Files.readAllLines(toBeRewritenTestFile.toPath()));
		Assert.assertFalse(fileContent.isEmpty());
		Assert.assertEquals(GREAT_SUCCESS, fileContent.get(0));
		
	}

}
