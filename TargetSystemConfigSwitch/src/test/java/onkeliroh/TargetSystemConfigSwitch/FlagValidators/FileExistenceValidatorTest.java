/**
 * 
 */
package onkeliroh.TargetSystemConfigSwitch.FlagValidators;

import java.io.File;

import org.junit.*;

import com.beust.jcommander.ParameterException;

public class FileExistenceValidatorTest {
	String testFilePath = "";
	File testFile;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		testFile = File.createTempFile("fileExistenceValidatorTestFile", ".txt");
		testFilePath = testFile.getAbsolutePath();
	}

	/**
	 * cleans up after testing
	 */
	@After
	public void cleanUp() {
		testFile.delete();
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FlagValidators.FileExistenceValidator#validate(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testValidate() throws Exception {
		try {
			FileExistenceValidator fev = new FileExistenceValidator();
			fev.validate("TestFile", testFilePath);
		} catch (ParameterException e) {
			Assert.fail(e.toString());
		}
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FlagValidators.FileExistenceValidator#validate(java.lang.String, java.lang.String)}.
	 */
	@Test(expected = ParameterException.class)
	public void testValidateFail() throws Exception {
		new FileExistenceValidator().validate("TestFile", testFilePath.substring(0, testFilePath.length() - 2));
	}

}
