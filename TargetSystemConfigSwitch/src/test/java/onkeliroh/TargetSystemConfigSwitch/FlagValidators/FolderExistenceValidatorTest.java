package onkeliroh.TargetSystemConfigSwitch.FlagValidators;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.beust.jcommander.ParameterException;

/**
 * @author onkeliroh
 */
public class FolderExistenceValidatorTest {
	String folderPath = "";
	File folderTestFile = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUpBefore() throws Exception {
		folderTestFile = File.createTempFile("tmp", ".txt");
		folderPath = folderTestFile.getAbsolutePath();
		folderPath = folderPath.substring(0, folderPath.lastIndexOf(File.separator));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDownAfter() throws Exception {
		folderTestFile.delete();
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FlagValidators.FolderExistenceValidator#validate(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testValidate() throws Exception {
		FolderExistenceValidator fev = new FolderExistenceValidator();
		fev.validate("", folderPath);
	}

	/**
	 * Test method for
	 * {@link onkeliroh.TargetSystemConfigSwitch.FlagValidators.FolderExistenceValidator#validate(java.lang.String, java.lang.String)}.
	 */
	@Test(expected = ParameterException.class)
	public void testValidateFail() throws Exception {
		FolderExistenceValidator fev = new FolderExistenceValidator();
		fev.validate("", folderPath + "hurpdurp");
	}

}
