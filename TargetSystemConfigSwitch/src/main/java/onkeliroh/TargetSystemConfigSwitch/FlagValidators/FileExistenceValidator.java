package onkeliroh.TargetSystemConfigSwitch.FlagValidators;

import java.io.File;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class FileExistenceValidator implements IParameterValidator {
	public void validate(String name, String value) throws ParameterException {
		File f = new File(value);
		if (f.exists()) {
			if (f.isFile()) {
				return;
			}
			throw new ParameterException(String.format("%s is not a file", value));
		}
		throw new ParameterException(String.format("File at %s does not exsist", value));
	}
}
