package onkeliroh.TargetSystemConfigSwitch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import onkeliroh.TargetSystemConfigSwitch.FlagValidators.FileExistenceValidator;
import onkeliroh.TargetSystemConfigSwitch.FlagValidators.FolderExistenceValidator;
import onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl.FileManager;
import onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl.FileRegExPattern;
import onkeliroh.TargetSystemConfigSwitch.TrayManager.SystemTrayManager;

/**
 * @author onkeliroh
 *
 */
public class App {

	@SuppressWarnings("javadoc")
	public List<FileInfo> sourceFbscFileInfos = new ArrayList<FileInfo>();

	@SuppressWarnings("javadoc")
	public FileInfo currentFbscConfig;

	@Parameter(names = "--help", help = true)
	private boolean help;

	/**
	 * A cli parameter representing the path to the fbsc configurations
	 */
	@Parameter(names = "--configFbscSource", description = "The source folder for all your FBSC configurations", required = false, validateWith = FolderExistenceValidator.class)
	public final String configFbscSourceFolderPath = null;

	/**
	 * A cli parameter representing the path to the fbsc configuration target file
	 */
	@Parameter(names = "--configFbscTarget", description = "The target file for your FBSC configuration", required = false, validateWith = FileExistenceValidator.class)
	public final String configFbscTargetFilePath = null;
	/**
	 * A cli parameter representing the path to the dash configurations
	 */
	@Parameter(names = "--configDashSource", description = "The source folder for all your Dash configurations", required = false, validateWith = FolderExistenceValidator.class)
	public final String configDashSourceFolderPath = null;

	/**
	 * A cli parameter representing the path to the fbsc configuration target file
	 */
	@Parameter(names = "--configDashTarget", description = "The target file for your Dash configuration", required = false, validateWith = FileExistenceValidator.class)
	public final String configDashTargetFilePath = null;

	@SuppressWarnings("javadoc")
	public static void main(String[] args) {
		App app = new App();
		new JCommander(app, args);
		app.run();
	}

	private void run() {
		 try {
		 collectFileInformations();
			SystemTrayManager.createSystemTrayManager(sourceFbscFileInfos, currentFbscConfig);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void collectFileInformations() throws IOException {
		File sourceFolder = new File(configFbscSourceFolderPath);

		FileManager fm = FileManager.getInstance();

		for (File source : sourceFolder.listFiles()) {
			if (fm.scanFileForPattern(source.getAbsolutePath(), FileRegExPattern.FBSCConfig)) {
				sourceFbscFileInfos.add(new FileInfo(source.getAbsolutePath(),
						fm.getStringMatchingPatternFromFile(source.getAbsolutePath(), FileRegExPattern.FBSCConfig)));
			}
		}

		if (fm.scanFileForPattern(configFbscTargetFilePath, FileRegExPattern.FBSCConfig)) {
			currentFbscConfig = new FileInfo(configFbscSourceFolderPath,
					fm.getStringMatchingPatternFromFile(configFbscTargetFilePath, FileRegExPattern.FBSCConfig));
		} else {
			throw new ParameterException("The given targetfile does not contain the nessesary informations");
		}
	}
}
