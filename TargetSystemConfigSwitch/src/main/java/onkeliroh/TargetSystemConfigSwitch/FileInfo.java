package onkeliroh.TargetSystemConfigSwitch;

/**
 * @author onkeliroh
 *
 */
public class FileInfo {
	/**
	 * 
	 */
	private String path;
	private String label;

	/**
	 * @param path
	 * @param label
	 */
	public FileInfo(String path, String label) {
		this.path = path;
		this.label = label;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FileInfo [path=" + path + ", label=" + label + "]";
	}
}