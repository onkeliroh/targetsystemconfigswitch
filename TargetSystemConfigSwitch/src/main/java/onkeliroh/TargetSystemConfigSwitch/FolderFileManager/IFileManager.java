package onkeliroh.TargetSystemConfigSwitch.FolderFileManager;

import java.io.IOException;

import onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl.FileRegExPattern;

/**
 * @author onkeliroh
 *
 */
public interface IFileManager {

	/**
	 * @param filePath
	 * @param pattern
	 * @return true if pattern found
	 * @throws IOException
	 */
	public boolean scanFileForPattern(String filePath, String pattern) throws IOException;

	/**
	 * search for a pattern in a file and returns the first matching line if not
	 * successful returns null
	 * 
	 * @param filePath
	 * @param pattern
	 * @return returns first found string
	 * @throws IOException
	 */
	public String getStringMatchingPatternFromFile(String filePath, String pattern) throws IOException;

	/**
	 * replaces a line within a file by searching for the given pattern and
	 * replacing the first found line
	 * 
	 * @param filePath
	 * @param pattern
	 * @param newString
	 * @throws IOException
	 */
	public void replaceStringInFile(String filePath, String pattern, String newString) throws IOException;

	/**
	 * @see #scanFileForPattern(String, String)
	 * @param absolutePath
	 * @param fbscConfig
	 * @return true if pattern found
	 * @throws IOException
	 */
	public boolean scanFileForPattern(String absolutePath, FileRegExPattern fbscConfig) throws IOException;

	/**
	 * @see #getStringMatchingPatternFromFile(String, String)
	 * @param absolutePath
	 * @param fbscConfig
	 * @return returns first found string
	 * @throws IOException
	 */
	public String getStringMatchingPatternFromFile(String absolutePath, FileRegExPattern fbscConfig) throws IOException;
}