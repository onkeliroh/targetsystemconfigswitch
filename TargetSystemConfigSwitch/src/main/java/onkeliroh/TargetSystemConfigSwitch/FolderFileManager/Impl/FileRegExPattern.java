package onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl;

/**
 * @author onkeliroh
 * A collection of patterns used to match certain informations
 */
public enum FileRegExPattern {
	/**
	 * A pattern to scan an fbsc configuration for its target system
	 */
	FBSCConfig("[A-Z]{2,3}(-|_)[A-Z]{2,3}"),
	/**
	 * A pattern to scan a dash configuration fot its target dash name
	 */
	DashConfig();
	
	private final String pattern;
	
	private FileRegExPattern(){
		this.pattern = null;
	}
	private FileRegExPattern(final String pattern){
		this.pattern = pattern;
	}
	
	@Override
	public String toString(){
		return this.pattern;
	}
}
