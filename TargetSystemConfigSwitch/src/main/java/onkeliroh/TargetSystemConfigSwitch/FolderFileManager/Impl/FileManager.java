package onkeliroh.TargetSystemConfigSwitch.FolderFileManager.Impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import onkeliroh.TargetSystemConfigSwitch.FolderFileManager.IFileManager;

/**
 * @author onkeliroh
 *
 */
public class FileManager implements IFileManager {

	private static FileManager fm;

	/**
	 * returns an instance of the FileManager class
	 * @return a FileManager instance
	 */
	public static FileManager getInstance() {
		if (fm == null) {
			fm = new FileManager();
		}
		return fm;
	}

	
	public boolean scanFileForPattern(String filePath, String pattern) throws IOException {
		File file = new File(filePath);
		if (file.exists() && file.isFile()) {
			try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
				String line = bf.readLine();
				while (line != null) {
					boolean matches = Pattern.matches(pattern, line);
					if (matches) {
						return true;
					}
					line = bf.readLine();
				}
			} catch (IOException e) {
				throw e;
			}
		} else {
			throw new IOException("Wrong path. File does not exsist");
		}
		return false;
	}

	
	public String getStringMatchingPatternFromFile(String filePath, String pattern) throws IOException {
		File file = new File(filePath);
		if (file.exists() && file.isFile()) {
			try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
				String line = bf.readLine();
				while (line != null) {
					boolean matches = Pattern.matches(pattern, line);
					if (matches) {
						return line;
					}
					line = bf.readLine();
				}
			} catch (IOException e) {
				throw e;
			}
		} else {
			throw new IOException("Wrong path. File does not exsist");
		}
		return null;
	}


	public void replaceStringInFile(String filePath, String pattern, String newString) throws IOException {
		File file = new File(filePath);
		List<String> fileContent = new ArrayList<>(Files.readAllLines(file.toPath()));

		for (int i = 0; i < fileContent.size(); i++) {
			if (Pattern.matches(pattern, fileContent.get(i))) {
				fileContent.set(i, newString);
				break;
			}
		}

		Files.write(file.toPath(), fileContent);
	}

	public boolean scanFileForPattern(String absolutePath, FileRegExPattern fbscConfig) throws IOException {
		return scanFileForPattern(absolutePath, fbscConfig.toString());
	}

	public String getStringMatchingPatternFromFile(String absolutePath, FileRegExPattern fbscConfig) throws IOException {
		return getStringMatchingPatternFromFile(absolutePath, fbscConfig.toString());
	}

}
