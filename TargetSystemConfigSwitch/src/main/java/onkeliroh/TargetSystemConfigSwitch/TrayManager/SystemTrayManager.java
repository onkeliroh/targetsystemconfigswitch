package onkeliroh.TargetSystemConfigSwitch.TrayManager;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.ImageIcon;

import onkeliroh.TargetSystemConfigSwitch.FileInfo;

/**
 * @author onkeliroh A class to create a systemTray presence
 */
public class SystemTrayManager {

	private static TrayIcon trayIcon;
	private static PopupMenu menu;
	private static ImageIcon icon;

	/**
	 * @param sourceFileInfos
	 * @param currentDevSysConfig
	 */
	public static void createSystemTrayManager(List<FileInfo> sourceFileInfos, FileInfo currentDevSysConfig) {
		if (!SystemTray.isSupported()) {
			System.err.println("The system does not supports system tray.");
			System.exit(0);
		}
		createSystemTrayMenu(sourceFileInfos, currentDevSysConfig);
	}

	private static void createSystemTrayMenu(List<FileInfo> sourceFileInfos, FileInfo currentDevSysConfig) {
		menu = new PopupMenu();
		appendDevSystems(sourceFileInfos, currentDevSysConfig);
		menu.addSeparator();
		appendMenuItem("Exit").addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		createSystemTrayIcon();
	}

	private static MenuItem appendMenuItem(String label, PopupMenu menu) {
		MenuItem newMenuItem = new MenuItem(label);
		menu.add(newMenuItem);

		return newMenuItem;
	}

	private static MenuItem appendMenuItem(String label) {
		return appendMenuItem(label, menu);
	}

	private static void createSystemTrayIcon() {
		icon = new ImageIcon("images/icone.gif");
		trayIcon = new TrayIcon(icon.getImage(), null, menu);
		trayIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					SystemTray.getSystemTray().remove(trayIcon);
				}
			}
		});
		AddToSystemTray();
	}

	private static void AddToSystemTray() {
		try {
			SystemTray.getSystemTray().add(trayIcon);
		} catch (AWTException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private static void appendDevSystems(List<FileInfo> sourceFileInfos, FileInfo currentDevSysConfig) {
		menu.add(new MenuItem(currentDevSysConfig.getLabel()));
		menu.addSeparator();
		for (FileInfo fileInfo : sourceFileInfos) {
			System.out.println(fileInfo);
			menu.add(new MenuItem(fileInfo.getLabel()));
		}
	}
}